package com.example.affirmations
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.Shapes
import com.example.affirmations.ui.theme.Typography
import com.example.affirmations.ui.theme.viewmodel.MainViewModel

class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      AffirmationApp()

      // TODO 5. Show screen


    }
  }
}

@Composable
fun AffirmationApp(mainViewModel: MainViewModel = viewModel()) {
  // TODO 4. Apply Theme and affirmation list
  AffirmationsTheme {
    val affirmationUiState by mainViewModel.uiState.collectAsState()
    AffirmationList(affirmationList = affirmationUiState,
      modifier = Modifier.background(color = MaterialTheme.colors.background))
  }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
  // TODO 3. Wrap affirmation card in a lazy column
  LazyColumn(modifier = modifier) {
    items(affirmationList) { affirmation -> AffirmationCard(affirmation = affirmation, modifier=Modifier.padding(all= 4.dp)) }
  }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
  // TODO 1. Your card UI
  var expanded by remember { mutableStateOf(false) }
  Card(modifier=modifier,border = BorderStroke(
    2.dp, color= MaterialTheme.colors.secondary), elevation = 2.dp) {
    Column(modifier = Modifier
      .animateContentSize(
        animationSpec = spring(
          dampingRatio = Spring.DampingRatioMediumBouncy,
          stiffness = Spring.StiffnessLow
        )
      )) {
      Row(modifier = Modifier
        .fillMaxSize(),
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = Alignment.CenterVertically,
      ) {
        Image(
          painter = painterResource(id = affirmation.imageResourceId),
          contentDescription = stringResource(id = affirmation.stringResourceId),
          modifier = Modifier
            .weight(1.5f, true)
            .padding(7.dp)
            .border(4.dp, color = MaterialTheme.colors.primaryVariant, shape = Shapes.small)

        )
        Text(
          text = stringResource(id = affirmation.stringResourceId),
          style= Typography.body1,
          color = MaterialTheme.colors.secondary,
          modifier = Modifier
            .weight(1.5f)
            .padding(10.dp)
        )

        ItemButton(
          expanded = expanded,
          onClick = { expanded = !expanded},
        )
      }
      if (expanded) {
        Description(affirmation = affirmation)}

    }


  }
}

@Composable
fun ItemButton(expanded: Boolean,
               onClick: () -> Unit,
               modifier: Modifier = Modifier
) {
  IconButton(onClick = onClick) {
    Icon(
      imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
      tint = MaterialTheme.colors.secondary,
      contentDescription = "", modifier = Modifier.size(23.dp)
    )
  }
}

@Composable
fun Description(affirmation: Affirmation) {
  Column(
    modifier = Modifier.padding(
      start = 8.dp,
      top = 4.dp,
      bottom = 8.dp,
      end = 4.dp
    )
  ) {
    Text(
      text = "Description: ",
      color = MaterialTheme.colors.primary,
      modifier = Modifier
        .padding(2.dp)
    )

    Text(
      text = stringResource(id = affirmation.stringDescriptionID),
      modifier = Modifier
        .padding(2.dp),
      overflow = TextOverflow.Ellipsis,
      maxLines = 2
    )
    Row(
      modifier = Modifier
        .padding(end = 10.dp)
    ) {
      Spacer(modifier = Modifier.weight(8f))
      val mContext = LocalContext.current
      Button(onClick = {
        val intent = Intent(mContext, DetailActivity::class.java)
        intent.putExtra("id", affirmation.id)
        mContext.startActivity(intent)
      }
      ) {

        Icon(
          imageVector = Icons.Filled.Add,
          tint = MaterialTheme.colors.secondary,
          contentDescription = "See MOre"
        )
        Text(
          text = "See More",
          color = MaterialTheme.colors.surface,
          modifier = Modifier
            .padding(start = 5.dp)

        )

      }
    }


  }
}


@Preview
@Composable
private fun AffirmationCardPreview() {
  // TODO 2. Preview your card
  AffirmationCard (        affirmation = Affirmation(
    R.string.affirmation1,
    R.drawable.image1,
    R.string.description1,
    1
  )
  )

}
